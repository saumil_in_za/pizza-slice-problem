package hashcode.practice;

import org.apache.commons.lang3.time.StopWatch;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.security.Permission;

import static org.junit.Assert.*;

/**
 * Created on 2017/02/22.
 *
 * @author saumil
 */
public class FileReaderTest extends BaseAppTest {
    private ByteArrayOutputStream outputContent, errorContent;
    private PrintStream outputConsole, errorConsole;

    @Before
    public void setUp() throws Exception {
        System.setSecurityManager(new NoExitSecurityManager());

        outputContent = new ByteArrayOutputStream();
        outputConsole = System.out;
        System.setOut(new PrintStream(outputContent));

        errorContent = new ByteArrayOutputStream();
        errorConsole = System.err;
        System.setErr(new PrintStream(errorContent));
    }

    @After
    public void tearDown() throws Exception {
        System.setOut(outputConsole);
        System.setErr(errorConsole);

        System.setSecurityManager(null);
    }

    @Test
    public void shouldReadFileSuccessfully() throws Exception {
        String inputFileName = getResourceFilePath("big.in");
        assertTrue(inputFileName.length() > 0);

        String[] args = {"-f", inputFileName};
        try {
            App.main(args);
        } catch (ExitException ex){
            assertEquals("Exit Status", 0, ex.status);
        }
    }

    static class ExitException extends SecurityException {
        private final int status;

        ExitException(int status) {
            super("Application exited");
            this.status = status;
        }
    }

    private static class NoExitSecurityManager extends SecurityManager {
        @Override
        public void checkPermission(Permission perm) {
        }

        @Override
        public void checkPermission(Permission perm, Object context) {
        }

        @Override
        public void checkExit(int status) {
            super.checkExit(status);
            throw new ExitException(status);
        }
    }
}