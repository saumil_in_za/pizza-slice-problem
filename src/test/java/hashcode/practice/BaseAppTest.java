package hashcode.practice;

import java.io.File;
import java.net.URL;

/**
 * Created on 2017/02/22.
 *
 * @author saumil
 */
public class BaseAppTest {
    protected String getResourceFilePath(final String fileName) {
        ClassLoader classLoader = getClass().getClassLoader();
        URL fileUrl = classLoader.getResource(fileName);
        if (fileUrl != null) {
            File file = new File(fileUrl.getFile());
            return file.getAbsolutePath();
        }
        return "";
    }
}
