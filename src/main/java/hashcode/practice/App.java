package hashcode.practice;

import org.apache.commons.cli.ParseException;

import java.io.File;
import java.io.IOException;

import static hashcode.practice.CommandLineOptions.getCommandLineOptions;

public class App {

    public static void main(String[] args) {
        if (args == null || args.length < 1) {
            System.err.println("too few arguments");
            CommandLineOptions.printHelp();
            return;
        }

        try {
            CommandLineOptions.initialize(args);
        } catch (ParseException ex) {
            System.err.println("Unable to parse options - " + ex.getMessage());
            CommandLineOptions.printHelp();
            return;
        }

        if (getCommandLineOptions().isHelp()) {
            CommandLineOptions.printHelp();
            return;
        }

        final String inputFileName = getCommandLineOptions().getInputFile();
        if (inputFileName != null && inputFileName.length() > 0) {
            readFile(inputFileName);
            // TODO Do something with the file
        }

        System.exit(0);
    }

    private static void readFile(String inputFileName) {
        FileReader reader = new FileReader(new File(inputFileName));
        try {
            reader.read();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
