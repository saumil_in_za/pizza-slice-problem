package hashcode.practice;

import org.apache.commons.cli.*;

import static java.lang.String.format;

/**
 * Created on 2017/02/22.
 *
 * @author saumil
 */
public class CommandLineOptions {
    private static final String JAR_NAME = "pizza-slice-solver";
    private static final Options optionsToParse = buildOptions();
    private static final CommandLineOptions options = new CommandLineOptions();
    private boolean isInitialized;

    private boolean help = false;
    private String inputFileName;

    public static CommandLineOptions getCommandLineOptions() {
        if (!options.isInitialized) {
            throw new IllegalStateException("Command-Line Options has not been initialized");
        }
        return options;
    }

    public static void initialize(String[] args) throws ParseException {
        CommandLineParser parser = new DefaultParser();
        CommandLine line = parser.parse(optionsToParse, args);
        if (line.getOptions().length > 2) {
            final String errorMsg = "Incorrect number of options provided";
            System.err.println(errorMsg);
            throw new ParseException(errorMsg);
        }

        options.help = line.hasOption("help");
        if (line.hasOption("file")) {
            options.inputFileName = line.getArgList().get(0);
        }
        options.isInitialized = true;
    }

    public static void printHelp() {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp(JAR_NAME, optionsToParse);
    }

    public String getInputFile() {
        return inputFileName;
    }

    public boolean isHelp() {
        return help;
    }

    private static void printOptions(CommandLine line) {
        for (Option opt : line.getOptions()) {
            String optionName = opt.getLongOpt() != null ? "-" + opt.getLongOpt() : opt.getOpt();
            System.out.println(format("option = -%s, value = %s", optionName, opt.getValue()));
        }
        System.out.println(System.getProperty("line.separator"));
    }

    private static Options buildOptions() {
        Option help = Option.builder("h")
                .longOpt("help").desc("print this help")
                .build();

        Option fileName = Option.builder("f")
                .longOpt("file").desc("input file")
                .build();

        return new Options()
                .addOption(fileName)
                .addOption(help);
    }
}
