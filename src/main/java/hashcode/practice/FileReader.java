package hashcode.practice;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.CharBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.util.ArrayList;
import java.util.List;

/**
 * Created on 2017/02/22.
 *
 * @author saumil
 */
public class FileReader {

    private final File file;
    private Charset encodingCharset = Charset.forName("US-ASCII");

    public static final List<Character> END_LINE_CHARS = new ArrayList<Character>() {{
        add('\r');
        add('\n');
    }};


    public FileReader(File file) {
        this.file = file;
    }


    public List<String> read() throws IOException {
        List<String> fileEntries = new ArrayList<>();

        RandomAccessFile in = new RandomAccessFile(file, "r");
        FileChannel channel = in.getChannel();
        try {
            MappedByteBuffer mappedByteBuffer = channel.map(FileChannel.MapMode.READ_ONLY, 0, file.length());

            CharsetDecoder decoder = encodingCharset.newDecoder();
            CharBuffer charBuffer = decoder.decode(mappedByteBuffer);

            StringBuilder strBuilder = new StringBuilder();
            for (int index = 0; index < charBuffer.limit(); index++) {
                char character = charBuffer.get(index);
                if (END_LINE_CHARS.contains(character)) {
                    fileEntries.add(strBuilder.toString());
                    strBuilder = new StringBuilder();
                    continue;
                }
                strBuilder.append(character);
                if (index == charBuffer.limit() - 1) {
                    fileEntries.add(strBuilder.toString());
                    break;
                }
            }
        } finally {
            channel.close();
        }
        return fileEntries;
    }
}
