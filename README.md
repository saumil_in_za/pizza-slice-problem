## Pizza Slice Solver

A command-line client written in Java 8 to read, parse and attend Pizza slice problem ( Practice Problem ). Based on problem statement - accepted file data format
is specified below:

```
6 7 1 5
TMMMTTT
MMMMTMM
TTMTTMT
TMMTMMM
TTTTTTM
TTTTTTM
```
### Assembly ###

#### Pre-requisites ####

* Unzip tool
* Apache Maven

#### Build ####
* Unzip the source code into destination directory
```
unzip pizza-slice-solver.zip -d ${DESTINATION_DIR}
cd ${DESTINATION_DIR}
```

* Build the application using [Maven](http://maven.apache.org/)
```
mvn clean pacakge -DskipTests=true
```
* Run test cases
```
mvn clean test
```

### Usage ###

```bash
$> java -jar pizza-slice-solver.jar --help
usage: pizza-slice-solver
 -f,--file   input file
 -h,--help   print this help
```

### Program Execution ###

#### Pre-requisites ####

Java Runtime Environment ( JRE ) is required to be installed on the machine before executing below mentioned command.

* Open Shell/Command Prompt and navigate to destination directory where sourcecode has been built
* Copy JAR file to desired location for use
```
cd ${PROJECT.DIR}/target
cp pizza-slice-solver.jar ${DESTINATION.DIR}
```
* Execute below commands to process files
```
java -jar pizza-slice-solver.jar --file /home/user/files/small.in
```

### References ###

Following libraries have been used to build artifact

*  [Apache Commons CLI](https://commons.apache.org/proper/commons-cli/) - Command-Line argument parsing
*  [Apache Commons Collections](https://commons.apache.org/proper/commons-collections/) - Collection related utilities

### Outstanding Tasks ###

* Extensive Test-case completion
